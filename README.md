# Vehint CI

Jobs, pipelines and helpers simplifying the compilation, testing, documentation and packaging of ROS 2 packages within Heudiasyc's vehicular platform.

## Basic usage

For most projects, putting the following in a file named `.gitlab-ci.yml` at the repository's root should be enough:

```yaml
include:
  - project: "limaanto/test_ci"
    ref: "jazzy"
    file: "pipelines/debug.yml"
```

This will compile the package(s) in debug mode and run `colcon test` on them.

## Advanced usage

### ROS package have git dependencies

Many ROS packages only have git dependencies, as opposed to debian dependencies, because they are proof of concept.
This use case is supported through the `GIT_DEPENDENCIES` variables.

By adding the following on top of `.gitlab-ci.yml`, the specified repositories

```yml
variables:
  GIT_DEPENDENCIES: "REPOSITORY1 REPOSITORY2"
```

That is, space-separated lists of repositories.
Repositories can be specified either:

- Relative to the current gitlab instance (i.e. [gitlab.utc.fr](https://gitlab.utc.fr)) as `NAMESPACE/REPOSITORY!REVISION`
- With a full URL (which is useful for dependencies on Github) as `https://URL!REVISION`

Here the `REVISION` can refer to a branch, tag or commit SHA.

For example:

```yaml
variables:
  GIT_DEPENDENCIES: "hds_vehint/can_zoe_interfaces!jazzy https://github.com/OUXT-Polaris/boost_geometry_util!master"
```

**NOTE** though that this variable is meant for debugging purposes and not *normal* use: if you can manage your dependencies through rosdep, you absolutely should!

### Project is not in the `hds_vehint` namespace

The server these jobs run on are already configured for the hds_vehint namespace.
To configure runners in other namespaces, [learn to create your own runners](https://docs.gitlab.com/runner/install/docker.html) or ask the hds_vehint maintainers for help.

Your runners should check at least two conditions:

- Provide [docker executors](https://docs.gitlab.com/runner/register/index.html?tab=Docker) and advertise as such with the `docker` tag
- Support [custom build dir](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnerscustom_build_dir-section)

## How it works

The following jobs are provided:

- Resolving dependencies: [`jobs/prepare_image.yml`](jobs/prepare_image.yml)
- Compiling: [`jobs/build_debug.yml`](jobs/build_debug.yml)
- Testing: [`jobs/test.yml`](jobs/test.yml)
- Documenting: [`jobs/build_doc.yml`](jobs/build_doc.yml) and [`jobs/deploy_doc.yml`](jobs/deploy_doc.yml)
- Packaging: [`jobs/build_package.yml`](jobs/build_package.yml) and [`jobs/deploy_package.yml`](jobs/deploy_package.yml)

They are grouped in three pipelines:

- Compile in debug and test, adapted to proof of concepts: [`pipelines/debug.yml`](pipelines/debug.yml)
- Compile in release and deploy `.debs`: [`pipelines/package.yml`](pipelines/package.yml)
- Both of the above, useful for most packages: [`pipelines/complete.yml`](pipelines/complete.yml)
