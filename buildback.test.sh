#! /usr/bin/bash

set -xe

CI_BUILDS_DIR=$(mktemp -d)
CI_PROJECT_PATH="hds_vehint/can_zoe"
CI_REGISTRY_IMAGE=registry.gitlab.utc.fr/hds_vehint/can_zoe
CI_COMMIT_REF_SLUG=4-migrate-to-ros-2
CI_JOB_TOKEN=glpat-YAWYaksb_FiVWYUAhXV8
CI_SERVER_HOST=gitlab.utc.fr

GIT_DEPENDENCIES="hds_vehint/rclcpp_utils!jazzy hds_vehint/can_interface!8-migrate-to-ros2-humble hds_vehint/can_zoe_interfaces!1-migrate-to-ros-2"
__FORCE_GIT_DEPENDENCIES=true
ROS_DISTRO=jazzy
PYTHONWARNINGS="ignore:::setuptools.command.install,ignore:::setuptools.command.easy_install,ignore:::pkg_resources"
ROS_WORKSPACE=$CI_BUILDS_DIR/$CI_PROJECT_PATH
GIT_CLONE_PATH=$ROS_WORKSPACE/src
__IMAGE_NAME="${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
__BASE_GIT_URL="https://limaanto:$CI_JOB_TOKEN@$CI_SERVER_HOST"
__VEHINT_CI_BASE="limaanto/test_ci"
GIT_STRATEGY=clone

REPO="$ROS_WORKSPACE/__repo__"
DEPSREPO="$ROS_WORKSPACE/__deps_repo__"
ROSDEP="$ROS_WORKSPACE/__rosdep-deb__.yaml"

git clone $__BASE_GIT_URL/$CI_PROJECT_PATH $GIT_CLONE_PATH
pushd $GIT_CLONE_PATH
    git checkout $CI_COMMIT_REF_SLUG
popd
cd $ROS_WORKSPACE

# Install dependencies after the docker image build such as GIT_DEPENDENCIES if __FORCE_GIT_DEPENDENCIES is true
if [[ ! __FORCE_GIT_DEPENDENCIES ]]; then unset GIT_DEPENDENCIES; fi
mkdir -p $GIT_CLONE_PATH/__git_ci_dependencies__ && pushd $GIT_CLONE_PATH/__git_ci_dependencies__
for _GIT_DEP in $GIT_DEPENDENCIES
do
    IFS=! read repo revision <<< $_GIT_DEP

    # Repo is not an URL (does not start with http). We assume it is
    # the namespace/project combo and prepend a Git URL to it
    if [[ ! "$repo" =~ ^http* ]]
    then
        repo="$__BASE_GIT_URL/$repo"
    fi

    git clone -c advice.detachedHead=false $repo

    pushd $(basename "$repo" .git)
        git checkout $revision
        git submodule update --init
    popd
done
popd

rosdep update --rosdistro=$ROS_DISTRO
rosdep install --from-paths $ROS_WORKSPACE/src --ignore-src --default-yes

# Setup a local & offline deb repository and rosdep resolver
mkdir -p $REPO $DEPSREPO
touch $ROSDEP
printf "yaml file://$ROSDEP\n" > /etc/ros/rosdep/sources.list.d/10-ci-deb.list
declare -A DEB_NAME_PATH_MAP

for PKG_PATH in $(colcon list --topological-order --paths-only)
do
  pushd $PKG_PATH
    echo "============ Generating debian packaging scripts for '$PKG_PATH' ============"
    bloom-generate rosdebian --ros-distro=$ROS_DISTRO

    # Allow parallel building by bumping the compat level since ROS' bloom takes
    # a while to do so. See https://github.com/ros-infrastructure/bloom/pull/643
    echo 12 > debian/compat
    export DEB_BUILD_OPTIONS="parallel=$(nproc)"

    # At the end of this loop, built packages are installed using their paths so that rosdep knows them
    # This causes paths to be written instead of the package name in the control file so we keep track of package
    # names and paths to fix this manually (ex /WS/__repo__/ros-jazzy-geometry-utils_2.0.0-0jammy_amd64.deb -> ros-jazzy-geometry-utils)
    for key in "${!DEB_NAME_PATH_MAP[@]}"
    do
      sed -i "s#${DEB_NAME_PATH_MAP[$key]}#$key#g" debian/control
    done

    echo "============ Compiling '$PKG_PATH' into a .deb using debhelper's tools and generated scripts ============"
    fakeroot debian/rules binary

    # Separate "official" deb to be published from "private" git dependencies (DEPSREPO is not published)
    if [[ "$PKG_PATH" == *"__git_ci_dependencies__"* ]]
    then
      CURRENT_REPO="$DEPSREPO"
    else
      CURRENT_REPO="$REPO"
    fi

    # There should be only one .deb there but its name is hard to infer so we're using a wildcard
    DEB_PATH="$CURRENT_REPO/$(basename ../*.deb)"
    mv ../*.deb "$DEB_PATH"

    # If debug symbols were generated, move them to the repo as well
    if ls ../*.ddeb 1> /dev/null 2>&1;
    then
      mv ../*.ddeb "$CURRENT_REPO"
    fi

    # Install the generated deb so that the next topological package is well resolved
    echo "============ Installing the compiled '$PKG_PATH' .deb ============"
    apt-get install -y --allow-downgrades "$DEB_PATH"

    # Create a rosdep entry so that bloom can resolve this package during the next iteration
    DEB_NAME=$(dpkg-deb -f $DEB_PATH Package)
    DEB_NAME_PATH_MAP[$DEB_NAME]="$DEB_PATH"
    PKG_NAME=$(colcon list --names-only)
    echo "$PKG_NAME: {ubuntu: [$DEB_PATH]}" >> $ROSDEP
    rosdep update --rosdistro=$ROS_DISTRO
  popd
done

# Move the generated packages into $GIT_CLONE_PATH so artifacts can be exported
mv $REPO $GIT_CLONE_PATH
